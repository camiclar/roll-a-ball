using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public AudioSource pickupSound;

    private Rigidbody rb;
    private int count;
    private Vector2 movement;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);
    }

    void Update()
    {
        /*if( Input.GetButtonDown("Jump") )
        {
            rb.AddForce(new Vector3(0f, 300f, 0f));
        }*/
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movement.x = movementVector.x;
        movement.y = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winTextObject.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        Vector3 thisMovement = new Vector3(movement.x, 0.0f, movement.y);

        rb.AddForce(thisMovement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();

            pickupSound.Play();
        }
        if (other.tag == "PickUp")
        {
            other.gameObject.GetComponent<VacuumPickups>().MoveToPlayer(transform);
        }

    }

}
